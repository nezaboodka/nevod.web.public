//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { nonreactive } from 'reactronic'
import { languages } from './Languages'

export class NavigationLink {
  language: string
  page: string
  pageTopic: string

  constructor() {
    this.language = ''
    this.page = ''
    this.pageTopic = ''
  }

  static parse(path: string): NavigationLink {
    const result = new NavigationLink()
    result.parse(path)
    return result
  }

  parse(hashPath: string): void {
    let startPos = 0
    if (hashPath[startPos] === '#')
      startPos++
    let endPos = hashPath.indexOf('/', startPos + 1)
    if (endPos < 0)
      endPos = hashPath.length
    if (endPos > startPos) {
      let namePos = startPos
      if (hashPath[namePos] === '/')
        namePos++
      const languageName = hashPath.slice(namePos, endPos)
      const language = nonreactive(() => languages.all.find(x => x.name === languageName))
      if (language) {
        this.language = language.name
        startPos = endPos
      }
      else {
        this.language = nonreactive(() => languages.active.name)
      }
      endPos = hashPath.indexOf('/', startPos + 1)
      if (endPos < 0)
        endPos = hashPath.length
      if (endPos > startPos) {
        this.page = hashPath.slice(startPos, endPos)
        startPos = endPos
        this.pageTopic = hashPath.slice(startPos)
      }
    }
  }
}
