//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { isnonreactive } from 'reactronic'
import { Page } from './Page'

export interface DownloadLink {
  title: string
  link: string
}

export interface DownloadGroup {
  title: string
  links: DownloadLink[]
}

export class DownloadsPage extends Page {
  @isnonreactive readonly groups: DownloadGroup[]

  constructor(pathBase: string, groups: DownloadGroup[]) {
    super(pathBase)
    this.groups = groups
  }
}
