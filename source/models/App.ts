//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { ReactiveObject, reaction, Transaction, isnonreactive, nonreactive } from 'reactronic'
import { HashNavigation } from './HashNavigation'
import { Page } from './Page'
import { MarkdownPage } from './MarkdownPage'
import { DownloadsPage } from './DownloadsPage'
import { PlaygroundData, PlaygroundPage } from './playground/PlaygroundPage'
import { JobsPatterns, JobsText } from './playground/PlaygroundExample-Jobs'
import { HealthcarePatterns, HealthcareText } from './playground/PlaygroundExample-Healthcare'
import { languages } from './Languages'
import { NavigationLink } from './NavigationLink'

export const ProjectLink = 'https://github.com/nezaboodka/nevod'

export class App extends ReactiveObject {
  @isnonreactive readonly version: string
  @isnonreactive readonly navigation: HashNavigation

  @isnonreactive readonly homePage: Page;
  @isnonreactive readonly tutorialPage: MarkdownPage;
  @isnonreactive readonly referencePage: MarkdownPage;
  @isnonreactive readonly downloadsPage: DownloadsPage;
  @isnonreactive readonly playgroundPage: PlaygroundPage;
  @isnonreactive readonly pages: Page[];
  activePage: Page;

  constructor(version: string) {
    super()
    this.version = version
    this.navigation = new HashNavigation()
    this.homePage = new Page('/home')
    this.tutorialPage = new MarkdownPage('/tutorial')
    this.referencePage = new MarkdownPage('/reference')
    this.downloadsPage = new DownloadsPage('/downloads', [
      { title: 'Windows', links: [
        { title: 'zip (x64)', link: 'https://github.com/nezaboodka/nevod/releases/latest/download/negrep-win-x64.zip' },
        { title: 'zip (x86)', link: 'https://github.com/nezaboodka/nevod/releases/latest/download/negrep-win-x86.zip' }
      ]},
      { title: 'Linux', links: [
        { title: 'deb', link: 'https://github.com/nezaboodka/nevod/releases/latest/download/negrep-x86_64.deb' },
        { title: 'tar.gz', link: 'https://github.com/nezaboodka/nevod/releases/latest/download/negrep-rhel.6-x64.tar.gz' }
      ]},
      { title: 'macOS', links: [
        { title: 'tar.gz', link: 'https://github.com/nezaboodka/nevod/releases/latest/download/negrep-osx-x64.tar.gz' }
      ]}
    ])
    this.playgroundPage = new PlaygroundPage('/playground')
    this.pages = [this.homePage, this.tutorialPage, this.referencePage, this.downloadsPage, this.playgroundPage]
    this.activePage = this.pages[0]
    this.activePage.isActive = true
  }

  @reaction
  protected onLanguageUpdate(): void {
    const activeLanguage = languages.active

    nonreactive(() => {
      this.homePage.menuName = activeLanguage.homePage_MenuName
      this.homePage.title = activeLanguage.homePage_Title

      this.tutorialPage.menuName = activeLanguage.tutorialPage_MenuName
      this.tutorialPage.title = activeLanguage.tutorialPage_Title
      this.tutorialPage.contentUrl = `assets/docs/${activeLanguage.name}/tutorial.md`

      this.referencePage.menuName = activeLanguage.referencePage_MenuName
      this.referencePage.title = activeLanguage.referencePage_Title
      this.referencePage.contentUrl = `assets/docs/${activeLanguage.name}/reference.md`

      this.downloadsPage.menuName = activeLanguage.downloadsPage_MenuName
      this.downloadsPage.title = activeLanguage.downloadsPage_Title

      this.playgroundPage.menuName = activeLanguage.playgroundPage_MenuName
      this.playgroundPage.examples = new Map<string, PlaygroundData>([
        ['', { patterns: activeLanguage.playgroundPage_DefaultExample, text: activeLanguage.playgroundPage_DefaultText }],
        ['/jobs', { patterns: JobsPatterns, text: JobsText }],
        ['/healthcare', { patterns: HealthcarePatterns, text: HealthcareText }],
      ])
    })
  }

  @reaction
  protected onNavigationPathChange(): void {
    const path = this.navigation.path
    nonreactive(() => {
      const link = NavigationLink.parse(path)
      const newActiveLanguage = languages.all.find(x => x.name === link.language)
      if (newActiveLanguage)
        languages.active = newActiveLanguage
      const newActivePage = this.pages.find(x => x.pagePath === link.page)
      if (newActivePage) {
        newActivePage.topicPath = link.pageTopic
        newActivePage.isActive = true
        this.activePage = newActivePage
        this.pages.forEach(x => {
          if (x !== newActivePage)
            x.isActive = false
        })
      }
      else {
        // Navigation path doesn't correspond any page
        Transaction.standalone(() => this.navigation.navigate(this.homePage.getPageLink())) // causes call to onNavigationPathChange again
      }
    })
  }
}
