//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Grammar } from 'prismjs'

export const NevodGrammar: Grammar = {
  'comment': /\/\/.*|(?:\/\*[\s\S]*?(?:\*\/|$))/,
  'string': {
    pattern: /(?:"(?:""|[^"])*"(?!")|'(?:''|[^'])*'(?!'))!?\*?/,
    greedy: true,
    inside: {
      'string-attrs': /!$|!\*$|\*$/,
    },
  },
  'namespace': {
    pattern: /(@(?:namespace|пространство)\s+)[a-zA-Zа-яёА-ЯЁ0-9\-.]+(?=\s*\{)/,
    lookbehind: true,
  },
  'pattern': {
    pattern: /(@(?:pattern|шаблон)\s+)?#?[a-zA-Zа-яёА-ЯЁ0-9\-.]+(?:\s*\(\s*(?:~\s*)?[a-zA-Zа-яёА-ЯЁ0-9\-.]+\s*(?:,\s*(?:~\s*)?[a-zA-Zа-яёА-ЯЁ0-9\-.]*)*\))?(?=\s*=)/,
    lookbehind: true,
    inside: {
      'pattern-name': {
        pattern: /^#?[a-zA-Zа-яёА-ЯЁ0-9\-.]+/,
        alias: 'class-name',
      },
      'fields': {
        pattern: /\(.*\)/,
        inside: {
          'field-name': {
            pattern: /[a-zA-Zа-яёА-ЯЁ0-9\-.]+/,
            alias: 'variable',
          },
          'punctuation': /[,()]/,
          'operator': {
            pattern: /~/,
            alias: 'field-hidden-mark',
          },
        },
      },
    },
  },
  'search': {
    pattern: /(@(?:search|искать)\s+|#)[a-zA-Zа-яёА-ЯЁ0-9\-.]+(?:\.\*)?(?=\s*;)/,
    alias: 'function',
    lookbehind: true,
  },
  'keyword': /@(?:having|inside|namespace|outside|pattern|require|search|where|вне|внутри|где|искать|пространство|содержащий|требуется|шаблон)(?=[\s:;,+~!(){}\[\]]|$)/,
  'standard-pattern': {
    pattern: /(^|[\s:;,+~!(){}\[\]])(?:Alpha|AlphaNum|Any|Blank|End|LineBreak|Num|NumAlpha|Punct|Space|Start|Symbol|Word|WordBreak|Буквы|БуквыЦифры|ЗнакПрепинания|Конец|Любое|Начало|Пробел|Пропуск|РазделительСлов|ПереносСтроки|Символ|Слово|Цифры|ЦифрыБуквы)(?:\([a-zA-Zа-яёА-ЯЁ0-9\-.,\s+]*\))?(?=[\s:;,+~!(){}\[\]]|$)/,
    lookbehind: true,
    inside: {
      'standard-pattern-name': {
        pattern: /^[a-zA-Zа-яёА-ЯЁ0-9\-.]+/,
        alias: 'builtin',
      },
      'quantifier': {
        pattern: /(^|[\s:;,+~!(){}\[\]])\d+(?:\s*\+|\s*-\s*\d+)?(?!a-zA-Zа-яёА-ЯЁ)/,
        lookbehind: true,
        alias: 'number',
      },
      'standard-pattern-attr': {
        pattern: /[a-zA-Zа-яёА-ЯЁ0-9\-.]+/,
        alias: 'builtin',
      },
      'punctuation': /[,()]/,
    },
  },
  'quantifier': {
    pattern: /(^|[\s:;,+~!(){}\[\]])\d+(?:\s*\+|\s*-\s*\d+)?(?!a-zA-Zа-яёА-ЯЁ)/,
    lookbehind: true,
    alias: 'number',
  },
  'operator': [
    {
      pattern: /=/,
      alias: 'pattern-def',
    },
    {
      pattern: /&/,
      alias: 'conjunction',
    },
    {
      pattern: /~/,
      alias: 'exception',
    },
    {
      pattern: /\?/,
      alias: 'optionality',
    },
    {
      pattern: /[[\]]/,
      alias: 'repetition',
    },
    {
      pattern: /[{}]/,
      alias: 'variation',
    },
    {
      pattern: /_\*|[_+]/,
      alias: 'sequence',
    },
    {
      pattern: /\.{2,3}/,
      alias: 'span',
    },
  ],
  'field-capture': [
    {
      pattern: /([a-zA-Zа-яёА-ЯЁ0-9\-.]+\s*\()\s*[a-zA-Zа-яёА-ЯЁ0-9\-.]+\s*:\s*[a-zA-Zа-яёА-ЯЁ0-9\-.]+(?:\s*,\s*[a-zA-Zа-яёА-ЯЁ0-9\-.]+\s*:\s*[a-zA-Zа-яёА-ЯЁ0-9\-.]+)*(?=\s*\))/,
      lookbehind: true,
      inside: {
        'field-name': {
          pattern: /[a-zA-Zа-яёА-ЯЁ0-9\-.]+/,
          alias: 'variable',
        },
        'colon': /:/,
      },
    },
    {
      pattern: /[a-zA-Zа-яёА-ЯЁ0-9\-.]+\s*:/,
      inside: {
        'field-name': {
          pattern: /[a-zA-Zа-яёА-ЯЁ0-9\-.]+/,
          alias: 'variable',
        },
        'colon': /:/,
      },
    },
  ],
  'punctuation': /[:;,()]/,
  'name': /[a-zA-Zа-яёА-ЯЁ0-9\-.]+/
}
