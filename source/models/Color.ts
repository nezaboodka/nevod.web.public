//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { HslColor } from './NevodServiceApi'

export const DefaultSaturation = 80
export const DefaultLightness = 50

export abstract class Color {
  static readonly default: HslColor = { h: 0, s: 0, l: 50 }

  static toHslaString(color: HslColor, alpha: number = 1, lightness?: number, saturation?: number): string {
    const l = lightness !== undefined ? lightness : color.l
    const s = saturation !== undefined ? saturation : color.s
    return `hsla(${color.h}, ${s}%, ${l}%, ${alpha})`
  }

  static createNew(anotherColor: HslColor): HslColor {
    return {
      h: anotherColor.h,
      s: anotherColor.s,
      l: anotherColor.l
    }
  }

  static generate(count: number = 1): HslColor[] {
    const low = 0
    const high = 360
    const step = Math.floor((high - low) / count)
    const s = DefaultSaturation
    const l = DefaultLightness
    let left = low
    let right = low + step * Math.ceil(count / 2)
    const result: HslColor[] = []
    for (let i = 0; i < count; i++) {
      let h
      if (i % 2 === 0) {
        h = left
        left += step
      } else {
        h = right
        right += step
      }
      const color = {h, s, l}
      result.push(color)
    }
    return result
  }
}
