//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Language } from './Languages'
import { DefaultPatterns_RU, DefaultText_RU } from './playground/PlaygroundExample-Default'

export class Russian implements Language {
  readonly name = 'ru'

  readonly logoName = 'Невод'

  readonly displayName = 'Русский'
  readonly copyright = '© 2017 - 2022 · ООО Незабудка Софтвер · Минск, Беларусь'

  readonly homePage_MenuName = 'Главное'
  readonly homePage_Title = 'Поиск в тексте совпадений с шаблонами'
  homePage_IntroductionHtml(tutorialLink: string, referenceLink: string, playgroundLink: string, downloadsLink: string, projectLink: string): string {
    return `«Невод» — это одновременно <b>язык</b> и технология с <b>открытым исходным кодом</b> для поиска в тексте совпадений с шаблонами.
    «Невод» специально разработан для быстрого выявления сущностей и их отношений в текстах на естественном языке.<br />
    <br />
    Отличительные особенности «Невода»:<br />
    <br />
    <div><img src="assets/star-plain-blue.svg" />во много раз <b>быстрее</b>, чем регулярные выражения (RegExp);</div>
    <div><img src="assets/star-plain-blue.svg" />для <b>обычных пользователей</b>, не только для программистов;</div>
    <div><img src="assets/star-plain-blue.svg" />масшабируемость на <b>больших данных</b>;</div>
    <div><img src="assets/star-plain-blue.svg" />оперирование <b>над словами</b>, а не над символами, текста;</div>
    <div><img src="assets/star-plain-blue.svg" />поиск всех шаблонов <b>за один проход</b> по тексту;</div>
    <div><img src="assets/star-plain-blue.svg" /><b>нейтральность к языку</b> текста;</div>
    <div><img src="assets/star-plain-blue.svg" />создание <b>шаблонов на основе других шаблонов</b>;</div>
    <div><img src="assets/star-plain-blue.svg" />многократно используемые <b>пакеты шаблонов</b>.</div>
    <br />
    Начните изучение «Невода» с <a href="${tutorialLink}">учебника</a>. Позже обращайтесь к
    <a href="${referenceLink}">справочнику</a> по языку.<br />
    <br />
    Попробуйте «Невод» сами на общедоступной <a href="${playgroundLink}">демонстрационной площадке</a> или
    загрузите к себе <a href="${downloadsLink}">утилиту командной строки</a> для использования на локальном компьютере.<br />
    <br />
    Если Вы — инженер-программист, то можете также внести свой вклад в развитие проекта «Невод» с открытым исходным кодом,
    размещённого <a href="${projectLink}">на сайте GitHub</a>.`
  }

  readonly tableOfContentHtml = '<h1>Содержание</h1>'

  readonly tutorialPage_MenuName = 'Учебник'
  readonly tutorialPage_Title = 'Учебник по языку Невод'

  readonly referencePage_MenuName = 'Справочник'
  readonly referencePage_Title = 'Справочник по языку Невод'

  readonly downloadsPage_MenuName = 'Загрузить'
  readonly downloadsPage_Title = 'Загрузите утилиту Negrep'
  readonly downloadsPage_IntroductionHtml =
    `<p>Попробуйте <b>сами</b> технологию «Невод» путём загрузки и запуска утилиты <b>negrep</b>.<br/>
    <br/>
    Эта утилита, как следует из названия, эквивалентна утилите <b>grep</b> — хорошо известной утилите командной строки
    для ОС Linux, предназначенной для поиска в текстовых файлах строк, соответствующих регулярному выражению.
    Утилита negrep служит той же цели, но использует шаблоны «Невода» вместо регулярных выражений.<br/>
    <br/>
    Negrep является частью проекта «Невод» с открытым исходным кодом, размещенного
    <a href="https://github.com/nezaboodka/nevod">на сайте GitHub</a>. Ниже приведены ссылки для загрузки
    последней версии утилиты Negrep для ОС <b>Windows</b>, <b>Linux</b> и <b>macOS</b> в виде автономного архива,
    который можно распаковать и использовать немедленно без какой-либо процедуры установки.</p>`

  readonly playgroundPage_MenuName = 'Попробовать'
  readonly playgroundPage_PatternsLabelHtml = 'Ваши&nbsp;шаблоны:'
  readonly playgroundPage_TextToSearchLabel = 'Текст, в котором ищутся совпадения с шаблонами:'
  readonly playgroundPage_TextAreaPlaceholder = 'Введите текст'
  readonly playgroundPage_PatternsAreaPlaceholder = 'Введите текст шаблонов'

  playgroundPage_DescriptionAndExamplesHtml(projectLink: string, pageLink: string): string {
    return `Узнайте больше о пакете <b>Basic.np</b> с базовыми шаблонами на сайте <a href="${projectLink}">GitHub</a>
      или попробуйте один из следующих примеров:<br />
      <br />
      <div><a href="${pageLink}">основные шаблоны</a></div>
      <div><a href="${pageLink}/jobs">категоризация вакансий</a></div>
      <div><a href="${pageLink}/healthcare">извлечение медицинской информации</a></div>`
  }

  readonly playgroundPage_SearchButton = 'Поиск'
  readonly playgroundPage_ShowAllButton = 'Все показать'
  readonly playgroundPage_HideAllButton = 'Все скрыть'
  readonly playgroundPage_ErrorLabel = 'Ошибки:'
  readonly playgroundPage_NoTagsFound = 'Совпадений не найдено'
  readonly playgroundPage_ReadOnlyLabelHtml = 'неизменяемый&#8209;текст'
  playgroundPage_NumCharactersHtml(num: number): string {
    return `${num}&nbsp;символов`
  }
  playgroundPage_MaxCharactersHtml(num: number, max: number): string {
    return `${num}&nbsp;из&nbsp;${max} символов&nbsp;максимально`
  }
  readonly playgroundPage_DefaultExample = DefaultPatterns_RU
  readonly playgroundPage_DefaultText = DefaultText_RU
  readonly playgroundPage_PatternDefinitionTooLong = 'Слишком большое определение шаблонов'
  readonly playgroundPage_EmptyPatternsDefinition = 'Отсутствует определение шаблонов'
  readonly playgroundPage_TextToSearchTooLong = 'Слишком большой текст для поиска'
  readonly playgroundPage_EmptyTextToSearch = 'Отсутствует текст для поиска'
}
