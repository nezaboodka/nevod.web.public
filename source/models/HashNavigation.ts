//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { ReactiveObject, reaction, transaction } from 'reactronic'

export type NavigationParams = Record<string, string | undefined>

export class HashNavigation extends ReactiveObject {
  path: string
  params: NavigationParams

  private wasUpdatedFromBrowser: boolean

  constructor() {
    super()
    const hash = location.hash
    const p = parseLocation(hash)
    this.path = p.path || '#/'
    this.params = p.params
    this.wasUpdatedFromBrowser = false
    window.onhashchange = this.browserHashChanged
  }

  @transaction
  navigate(path: string, parameters: NavigationParams = {}): void {
    this.path = path
    this.params = parameters
    this.wasUpdatedFromBrowser = false
  }

  matches(path: string, exact: boolean = true): boolean {
    return this.path === path || !exact && this.path.startsWith(path)
  }

  @reaction
  protected updateBrowserHistory(): void {
    const path = this.path
    const params = this.params
    const hash = formatLocation(path, params)
    if (this.wasUpdatedFromBrowser)
      history.replaceState(undefined, '', hash)
    else
      history.pushState(undefined, '', hash)
  }

  @transaction
  protected browserHashChanged(): void {
    const hash = location.hash // don't exclude # symbol via .substring(1)
    const p = parseLocation(hash)
    this.path = p.path
    this.params = p.params
    this.wasUpdatedFromBrowser = true
  }
}

function parseLocation(location: string): { path: string, params: NavigationParams } {
  let path: string
  const params: NavigationParams = {}
  const searchPos = location.indexOf('?')
  if (searchPos > -1) {
    path = location.slice(0, searchPos)
    const search = location.slice(searchPos)
    const searchParams = new URLSearchParams(search) // makes decodeURIComponent
    searchParams.forEach((value, key) => params[key] = value)
  }
  else {
    path = location
  }
  return { path, params }
}

function formatLocation(path: string, params: NavigationParams): string {
  let result = path
  const searchParams: string[] = []
  for (const key in params) {
    const value = params[key]
    if (value)
      searchParams.push(`${key}=${encodeURIComponent(value)}`)
  }
  if (searchParams.length > 0)
    result += '?' + searchParams.join('&')
  return result
}
