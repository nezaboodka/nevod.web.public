//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Monitor } from 'reactronic'

export class Globals {
  static readonly Loading = Monitor.create('Loading', 1000, 250, 1)
}
