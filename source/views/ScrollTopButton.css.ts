//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { css } from '@emotion/css'
import { restyler } from 'reactronic-dom'
import { themes } from './Themes'

export const style = restyler(() => {
  return {
    ScrollTopButton: css`
      position: fixed;
      right: 10%;
      bottom: 5%;
      cursor: pointer;
      width: 0em;
      height: 0em;
      transition: width 0.2s ease, height 0.2s ease;

      &[rx-visible=true] {
        width: 3.5em;
        height: 3.5em;
      }

      svg {
        fill: ${themes.active.documentScrollTopButtonBackground};
        width: 100%;
        height: 100%;
      }
    `,
  }
})
