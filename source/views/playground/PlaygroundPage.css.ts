//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { css } from '@emotion/css'
import { restyler } from 'reactronic-dom'
import { themes } from '../Themes'
import { style as pageStyle } from '../Page.css'

const patternsMarginBottom = '2ch'

export const style = restyler(() => {
  const TitleMixin = css`
    font-size: 125%;
    margin-bottom: 0.5ch;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: flex-end;
    flex-wrap: wrap;
  `
  const PackageTabMixin = css`
    &[is-data-selected=true] {
      border-top: solid 3px ${themes.active.menuItemSelectedForeground};
      margin-top: -3px;
    }

    &[is-data-selected=false] {
      color: gray;
      cursor: pointer;

      &:hover {
        border-top: solid 3px ${themes.active.itemUnderPointerMarker};
        margin-top: -3px;
        color: lightgray;
      }
    }
  `

  return {
    PlaygroundPageView: css`
      font-size: 100%;
      line-height: 1.2;

      margin: 0;
      height: 100%;
      display: grid;
      grid-template-rows: auto 1fr auto 1fr;
      grid-template-columns: 62fr minmax(25ch, 30fr);
      column-gap: 5ch;

      code,
      pre {
        font-family: monospace, 'Courier New';
      }
    `,

    PatternsTitle: css`
      grid-row: 1;
      grid-column: 1 / span 1;
      ${TitleMixin};
    `,

    CustomPatterns: css`
      margin-right: 1.5ch;
      ${PackageTabMixin};
    `,

    Patterns: css`
      grid-row: 2;
      grid-column: 1;
      height: calc(100% - ${patternsMarginBottom});
      margin-bottom: ${patternsMarginBottom};
      min-height: 12ch;
      background-color: ${themes.active.playgroundTextAreaBackground};
      /* color: rgb(20, 52, 79); */
    `,

    RequiredPackageListTitle: css`
      grid-row: 1;
      grid-column: 2;
      ${TitleMixin};
    `,

    TextTitle: css`
      grid-row: 3;
      grid-column: 1;
      ${TitleMixin};
    `,

    Text: css`
      grid-row: 4;
      grid-column: 1;
      height: 100%;
      min-height: 20ch;
      background-color: ${themes.active.playgroundTextAreaBackground};
      text-shadow: none;
    `,

    BasicPatternsDescriptionCaption: css`
      grid-area: 1 / 2 / span 1 / span 1;
      ${TitleMixin};
    `,

    BasicPatternsDescription: css`
      grid-area: 2 / 2 / span 1 / span 1;
      margin-bottom: ${patternsMarginBottom};
    `,

    Result: css`
      grid-area: 4 / 2 / span 3 / span 1;
      height: 100%;
      display: flex;
      flex-direction: column;
    `,

    ControlsPanel: css`
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
    `,

    FindButtonContainer: css`
      display: flex;
      flex-direction: row;
      align-content: center;
    `,

    SearchIndicator: css`
      height: 4ch;
      width: 4ch;
      margin-right: 0.5ch;
      visibility: hidden;

      &[rx-active=true] {
        visibility: visible;
      }
    `,

    ControlButtonsContainer: css`
      font-size: 110%;
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      align-items: center;
    `,

    ControlButton: css`
      margin-bottom: 1ch;
      margin-right: 1ch;
      transition: color 0.3s ease, background-color 0.3s ease;
      box-shadow: 0 0 2px 0 #777;
      display: flex;
      justify-content: center;
      align-items: center;
      padding: 0.7ch 1.25ch;
      border: 0.025em solid #E0E0E0;
      border-radius: 0;
      background-color: transparent;
      color: #E0E0E0;
      cursor: pointer;
      border-radius: 0.25em;
      /* .UserSelectNoneMixin(); */

      &:hover {
        color: white;
        border-color: white;
      }

      &:active {
        background-color: hsl(0%, 0%, 30%);
        border-color: white;
      }

      &:last-child {
        margin-right: 0;
      }
    `,

    SearchResults: css`
      flex-grow: 1;
      position: relative;
      overflow-x: hidden;
      overflow-y: auto;
    `,

    SearchResultsScroll: css`
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
    `,

    ErrorMessage: css`
      color: white;
      font-size: 110%;
      border-left: 0.12ch solid rgb(253, 93, 93);
      padding-left: 1.5ch;
      word-break: break-word;
      /* .AppearAnimation(); */

    `,

    ErrorMessageLabel: css`
      color: rgb(253, 93, 93);
    `,

    Description: pageStyle.class.Description,
  }
})
