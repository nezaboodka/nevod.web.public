//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Div, RxDiv, RxSpan, Span, Sub } from 'reactronic-dom'
import { HighlightedText } from '../../models/HighlightedText'
import { HighlightedTextLayer } from '../../models/HighlightedTextLayer'
import { languages } from '../../models/Languages'
import { PlaygroundPage } from '../../models/playground/PlaygroundPage'
import { style } from './TagList.css'

export function TagListView(id: string, page: PlaygroundPage) {
  return (
    RxDiv(id, { page }, e => {
      e.className = style.class.TagList
      const text = page.text
      const tags = text.tagLayers
      tags.map(x => {
        TagListItem(`Tag-${page.lastSearchResult.revision}-${x.name}`, text, x)
      })
      if (page.lastSearchResult.response !== undefined && tags.length === 0)
        RxSpan('NoTagsFound', null, e => {
          e.textContent = languages.active.playgroundPage_NoTagsFound
        })
    })
  )
}

function TagListItem(id: string, text: HighlightedText, tag: HighlightedTextLayer) {
  return (
    Div(id, e => {
      e.className = style.class.TagListItemWrapper
      e.style.backgroundColor = (tag.isFocused || tag.isSelected) ? 'white' : 'transparent'
      Div('TagListItem', e => {
        e.className = style.class.TagListItem
        e.style.backgroundColor = tag.getButtonColorString()
        e.style.borderColor = tag.getButtonBorderColorString()
        e.style.color = tag.getButtonTextColorString()
        e.onclick = () => { tag.toggleSelected() }
        e.onpointerenter = () => { text.focusLayer(tag) }
        e.onpointerleave = () => {
          text.unfocusLayer()
        }
        Span('TagName', e => {
          e.textContent = tag.name
        })
        Sub('TagCount', e => {
          e.className = style.class.TagListItemCounter
          e.textContent = ` ${tag.count}`
        })
      })
    })
  )
}
