//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { css } from '@emotion/css'
import { restyler } from 'reactronic-dom'
import { themes } from '../Themes'

export const style = restyler(() => {
  const TagListItemMixin = css`
    transition: color 0.2s ease, background-color 0.2s ease, border-color 0.2s ease;
    border-radius: 0.25em;
  `

  return {
    TagList: css`
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      justify-items: center;
      /* .AppearAnimation(); */
    `,

    TagListItemWrapper: css`
      margin-right: 0.5ch;
      margin-bottom: 0.5ch;
      ${TagListItemMixin};

      &:active {
        background-color: ${themes.active.playgroundTagButtonBackground};
      }

      &:last-child {
        margin-right: 0;
      }
    `,

    TagListItem: css`
      padding: 0.7ch 1.25ch;
      cursor: pointer;
      word-break: break-all;
      ${TagListItemMixin};
      border-width: 0.05em;
      border-style: solid;
      /* .UserSelectNoneMixin(); */
    `,

    TagListItemCounter: css`
      font-size: 75%;
      position: relative;
      vertical-align: baseline;
      bottom: -0.25em;
      color: ${themes.active.playgroundTagButtonCounterForeground};
    `,
  }
})
