//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Div, Customize, RxDiv, Reaction, RxTextArea } from 'reactronic-dom'
import { HighlightedText } from '../../models/HighlightedText'
import { style } from './HighlightedTextArea.css'
import { themes } from '../Themes'
import { css } from '@emotion/css'
import { TransparentColor } from '../../models/HighlightedTextLayer'

const WidthMultiplier = 1.3
const HeightMultiplier = 1.5

export function HighlightedTextArea(id: string, placeholder: string, text: HighlightedText) {
  return (
    RxDiv(id, null, e => {
      e.className = style.class.HighlightedTextArea
      const backdrop = RxDiv('Backdrop', null, e => {
        e.className = style.class.Backdrop
        e.style.zIndex = '0'
        let zIndex = 1
        if (text.showWhiteSpaces) {
          HighlightedTextAreaLayer('WhiteSpaceLayer', zIndex, themes.active.playgroundWhiteSpaceForeground, text, (node, e) => {
            node.render()
            Reaction('ContentUpdater', null, () => {
              e.innerHTML = text.whiteSpacesLayerHtml
            })
          })
          zIndex++
        }
        if (text.tagLayers.length > 0) {
          text.tagLayers.map((tagLayer, i) => {
            HighlightedTextAreaLayer(`HighlightedTagLayer-${tagLayer.name}-${i}`, zIndex + i, themes.active.playgroundWhiteSpaceForeground, text, (node, e) => {
              node.render()
              e.classList.add(style.class.HighlightedTagLayer)
              RxDiv('HighlightedTextLayerContent', tagLayer, e => {
                const selectedOrFocused = tagLayer.isSelected || tagLayer.isFocused
                e.className = style.class.HighlightedTextLayerContent
                const highlightStyle = css`mark {
                  background-color: ${selectedOrFocused ? tagLayer.getTextMatchColorString() : TransparentColor};
                  border-color: ${selectedOrFocused ? tagLayer.getTextMatchBorderColorString() : TransparentColor};
                  color: ${selectedOrFocused ? 'grey' : TransparentColor};
                }`
                e.classList.add(highlightStyle)
                e.innerHTML = tagLayer.html
              })
            })
          })
        }
        zIndex += text.tagLayers.length
        if (text.isNevodPackage) {
          HighlightedTextAreaLayer('SyntaxLayer', zIndex, themes.active.playgroundTextAreaForeground, text, (node, e) => {
            node.render()
            Reaction('ContentUpdater', null, () => {
              e.innerHTML = text.syntaxLayerHtml
            })
          })
        }
      })
      RxTextArea('TextArea', null, e => {
        e.className = style.class.TextArea
        if (text.isNevodPackage)
          e.style.color = TransparentColor
        else
          e.style.color = themes.active.playgroundTextAreaForeground
        e.spellcheck = false
        e.wrap = 'soft'
        e.placeholder = placeholder
        e.value = text.text
        e.oninput = () => {
          text.setText(e.value)
        }
        e.onscroll = () => {
          if (backdrop.element) {
            backdrop.element.scrollTop = e.scrollTop
            backdrop.element.scrollLeft = e.scrollLeft
          }
        }
      })
    })
  )
}

export function HighlightedTextAreaLayer(id: string, zIndex: number, color: string, text: HighlightedText,
  customize: Customize<HTMLElement>) {

  return (
    Div(id, e => {
      e.className = style.class.HighlightedTextAreaLayer
      e.style.zIndex = zIndex.toString()
      const textSize = text.textSize
      e.style.width = `${textSize.width * WidthMultiplier}ch`
      e.style.height = `${textSize.height * HeightMultiplier}em`
      e.style.color = color
    }).wrapWith(customize)
  )
}
