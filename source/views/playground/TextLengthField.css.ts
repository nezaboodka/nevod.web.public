//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { css } from '@emotion/css'
import { restyler } from 'reactronic-dom'
import { themes } from '../Themes'

export const style = restyler(() => {
  return {
    TextLengthField: css`
      color: gray;
      font-size: 70%;
      text-align: right;

      &[is-data-invalid=true] {
        color: ${themes.active.playgroundTextLengthErrorForeground};
      }

      &[is-data-readonly=true] {
        color: ${themes.active.playgroundTextLengthForeground};
      }
    `,
  }
})
