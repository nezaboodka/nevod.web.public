//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { RxSpan } from 'reactronic-dom'
import { HighlightedText } from '../../models/HighlightedText'
import { languages } from '../../models/Languages'
import { style } from './TextLengthField.css'

export function TextLengthField(id: string, text: HighlightedText) {
  return (
    RxSpan(id, null, e => {
      e.className = style.class.TextLengthField
      if (text.isReadOnly) {
        e.innerHTML = languages.active.playgroundPage_ReadOnlyLabelHtml
      }
      else {
        const max = text.maxLength
        const current = text.text.length
        if (max > 0) {
          e.setAttribute('is-data-invalid', current > max ? 'true' : 'false')
          e.innerHTML = languages.active.playgroundPage_MaxCharactersHtml(current, max)
        }
        else {
          e.innerHTML = languages.active.playgroundPage_NumCharactersHtml(current)
        }
      }
    })
  )
}
