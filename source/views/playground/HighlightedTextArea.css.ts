//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { css } from '@emotion/css'
import { restyler } from 'reactronic-dom'
import { themes } from '../Themes'

const textAreaVerticalPadding = '5pt'
const textAreaSidePadding = '5pt'

export const style = restyler(() => {
  const TextBlockMixin = css`
    display: block;
    overflow-x: auto;
    overflow-y: auto;
  `
  const TextStyleMixin = css`
    padding: ${textAreaVerticalPadding} ${textAreaSidePadding};
    border: 0 solid transparent;
    font: 100% 'Consolas', monospace;
    line-height: 1.36;
    letter-spacing: 0.05em;
    white-space: pre;
    word-wrap: normal;
  `

  return {
    HighlightedTextArea: css`
      -webkit-text-size-adjust: none;
      position: relative;
      width: 100%;
      height: 100%;
    `,

    Backdrop: css`
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      pointer-events: none;
      ${TextBlockMixin};

      &::-webkit-scrollbar {
        display: none;
      }
    `,

    HighlightedTextAreaLayer: css`
      position: absolute;
      min-height: calc(100% - 2 * ${textAreaVerticalPadding});
      min-width: calc(100% - 2 * ${textAreaSidePadding});
      ${TextBlockMixin};
      ${TextStyleMixin};

      mark {
        margin-left: -2px;
        border-width: 1px;
        border-style: solid;
        border-radius: 0.25ch;
      }
    `,

    HighlightedTagLayer: css`
      transition: color 0.3s ease, background-color 0.3s ease, border-color 0.3s ease;

      mark {
        transition: color 0.3s ease, background-color 0.3s ease, border-color 0.3s ease;
        /* .AppearAnimation(); */
      }
    `,

    HighlightedTextLayerContent: css`
      width: 100%;
      height: 100%;
    `,

    TextArea: css`
      ${TextBlockMixin};
      ${TextStyleMixin};
      position: relative;
      z-index: 1000000;
      height: calc(100% - 2 * ${textAreaVerticalPadding});
      width: calc(100% - 2 * ${textAreaSidePadding});
      resize: none;
      background-color: transparent;
      caret-color: ${themes.active.playgroundTextAreaCaretColor};

      &:focus {
        outline: none;
      }

      &:focus:not([readOnly]) {
        box-shadow: 0 0 0 2px ${themes.active.menuItemSelectedForeground};
      }
    `,
  }
})
