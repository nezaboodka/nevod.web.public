//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { A } from 'reactronic-dom'
import { style } from './ScrollTopButton.css'

const ScrollTopButtonVisibilityDistance: number = 200

export function ScrollTopButton(scrollWindow: HTMLElement) {
  return (
    A('ScrollTopButton', e => {
      e.className = style.class.ScrollTopButton
      e.title = 'Top'
      document.addEventListener('scroll', () => {
        e.setAttribute('rx-visible', scrollWindow.scrollTop > ScrollTopButtonVisibilityDistance ? 'true' : 'false')
      })
      e.onclick = () => { scrollWindow.scrollTo(0, 0) }
      e.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
<path transform="rotate(-90) translate(-24)" d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373
 12-12-5.373-12-12-12zm-1.218 19l-1.782-1.75 5.25-5.25-5.25-5.25 1.782-1.75 6.968 7-6.968 7z" />
</svg>`
    })
  )
}
