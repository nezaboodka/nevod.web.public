//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { css } from '@emotion/css'
import { restyler } from 'reactronic-dom'
import { themes } from './Themes'

export const style = restyler(() => {
  return {
    MenuItem: css`
      margin: 0 2ch 0.25em 0;
      border-top: 0.1em solid transparent;

      &[rx-selected=true] {
        color: ${themes.active.menuItemSelectedForeground};
        border-top: 0.1em solid ${themes.active.menuItemSelectedForeground};
      }
    `,
  }
})
