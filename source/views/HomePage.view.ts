//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Div, RxDiv } from 'reactronic-dom'
import { PageView } from './Page.view'
import { style } from './Page.css'
import { App, ProjectLink } from '../models/App'
import { languages } from '../models/Languages'

export function HomePageView(app: App) {
  return (
    PageView(app.homePage, (node, e) => {
      node.render()
      RxDiv('Description', null, e => {
        e.className = style.class.Description
        e.innerHTML = languages.active.homePage_IntroductionHtml(
          app.tutorialPage.getPageLink(), app.referencePage.getPageLink(), app.playgroundPage.getPageLink(),
          app.downloadsPage.getPageLink(), ProjectLink)
      })
    }, (node, e) => {
      node.render()
      Div('Picture', e => {
        e.className = style.class.Picture
        e.innerHTML = '<img src="assets/nevod-play-3d.gif"/>'
      })
    })
  )
}
